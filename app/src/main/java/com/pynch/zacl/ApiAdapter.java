package com.pynch.zacl;

/**
 * Created by Hermann on 4/4/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Hermann on 1/12/2018.
 */

public class ApiAdapter extends BaseAdapter{
    private Activity activity;
    private LayoutInflater inflater;
    private List<Api> apiItems = new ArrayList<>();



    public ApiAdapter(Activity activity, List<Api> apiItems) {
        this.activity = activity;
        this.apiItems = apiItems;
    }


    @Override
    public int getCount() {
        return apiItems.size();
    }

    @Override
    public Object getItem(int i) {
        return apiItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.api, null);

        TextView value = convertView.findViewById(R.id.txtValue);

        TextView title = convertView.findViewById(R.id.txtTitle);





        // getting movie data for the row
        Api m = apiItems.get(i);
            // rating
            value.setText(Html.fromHtml(m.getValue()).toString());

            title.setText(Html.fromHtml(m.getKey()).toString());


        return convertView;
    }


}
