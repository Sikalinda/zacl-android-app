package com.pynch.zacl;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;
import com.pynch.zacl.CustomAdapter.CustomAdapter;
import com.pynch.zacl.CustomAdapter.ListModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DepartureActivity extends AppCompatActivity {

    RequestQueue requestQueue;
    private List<ListModel> flights = new ArrayList<>();
    private RecyclerView recyclerView;
    private CustomAdapter mAdapter;
    ListModel flight;
    SkeletonScreen skeletonScreen;





    TextView airportName, arrivalTime, departedTime, arrivedText, airportCode, flightCode;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_departure);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorTitle));
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.colorTitle), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        airportName = findViewById(R.id.airportName);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);



        // 2. set layoutManger
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // 3. create an adapter



        mAdapter = new CustomAdapter(flights);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        //skeleton libray
        skeletonScreen = Skeleton.bind(recyclerView)

                .adapter(mAdapter)
                .load(R.layout.loader)
                .show();


        getData();

        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void getData() {
        RequestQueue queue = Volley.newRequestQueue(DepartureActivity.this);
        String url = "http://www.zacl.co.zm/flights/";
        //
        // Initialize a new RequestQueue instance
        // prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {

                    // display response

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        JSONArray jsonArray = jsonObject.getJSONArray("enroute");
                        //
                        int index = 0;
                        while (jsonArray.length()>index){
                            Log.d("SIZE", String.valueOf(jsonArray.length()));
                            JSONObject data = (JSONObject) jsonArray.get(index);
                            JSONObject dataOrigin = data.getJSONObject("origin");
                            JSONObject dataDestination = data.getJSONObject("destination");
                            JSONObject dataActualArrivalTime = data.getJSONObject("estimated_arrival_time");
                            JSONObject dataActualDepartedTime = data.getJSONObject("estimated_departure_time");


                            //flight number
                            String flightNumber = data.getString("flightnumber");
                            //Status
                            String status = data.getString("status");
                            //airline
                            String airline = data.getString("airline");
                            String flightStatusString = status;
                            String[] flightStatus = flightStatusString.split("/");
                            //Flight Origin
                            String airlineNameOrigin = data.getString("airline");
                            String cityNameOrigin = dataOrigin.getString("city");
                            String airportNameOrigin = dataOrigin.getString("airport_name");
                            //Flight Destination
                            String airportNameDestination = dataOrigin.getString("airport_name");
                            String cityNameDestination = dataOrigin.getString("city");
                            String airportCode = dataOrigin.getString("code");
                            //
                            //actual arrived time
                            String arrivalTime = dataActualArrivalTime.getString("time");
                            //actual departed time
                            String departedTime = dataActualDepartedTime.getString("time");
                            flight = new ListModel(airportNameOrigin, arrivalTime , departedTime,flightStatus[0],airportCode,flightNumber,airline);
                            //

                            //
                            flights.add(flight);
                            mAdapter.notifyDataSetChanged();
                            index++;

                        }
                        //
                        hideSkeleton();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                },
                error -> {
                    Log.d("RESPONSE","Error here");
                    Toast.makeText(DepartureActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    Log.d("Error.Response", String.valueOf(error));
                    hideSkeleton();
                }
        );

// add it to the RequestQueue
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

        //

    }

    private void hideSkeleton() {
        skeletonScreen.hide();
        }
    

}
