package com.pynch.zacl;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import net.cachapa.expandablelayout.ExpandableLayout;

public class GetInTouchActivity extends AppCompatActivity implements View.OnClickListener{

    private ExpandableLayout expandableLayout1;
    private ExpandableLayout expandableLayout2;
    private ExpandableLayout expandableLayout3;
    private ExpandableLayout expandableLayout4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_in_touch);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        expandableLayout1 = findViewById(R.id.expandable_layout_1);
        expandableLayout2 = findViewById(R.id.expandable_layout_2);
        expandableLayout3 = findViewById(R.id.expandable_layout_3);
        expandableLayout4 = findViewById(R.id.expandable_layout_4);


        expandableLayout1.setOnExpansionUpdateListener((expansionFraction, state) -> Log.d("ExpandableLayout1", "State: " + state));
        expandableLayout2.setOnExpansionUpdateListener((expansionFraction, state) -> Log.d("ExpandableLayout2", "State: " + state));
        expandableLayout3.setOnExpansionUpdateListener((expansionFraction, state) -> Log.d("ExpandableLayout3", "State: " + state));
        expandableLayout4.setOnExpansionUpdateListener((expansionFraction, state) -> Log.d("ExpandableLayout4", "State: " + state));

        findViewById(R.id.expand_button).setOnClickListener(this);
        findViewById(R.id.expand_button2).setOnClickListener(this);
        findViewById(R.id.expand_button3).setOnClickListener(this);
        findViewById(R.id.expand_button4).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

      switch (view.getId()){
          case R.id.expand_button:
              if (expandableLayout1.isExpanded()) {
                  expandableLayout2.collapse();
                  expandableLayout3.collapse();
                  expandableLayout4.collapse();
                  expandableLayout1.expand();
              } else {

                  expandableLayout1.collapse();
              }

              break;
          case R.id.expand_button2:

              if (expandableLayout2.isExpanded()) {
                  expandableLayout1.collapse();
                  expandableLayout3.collapse();
                  expandableLayout4.collapse();
              } else {

                  expandableLayout2.expand();
              }

              break;
          case R.id.expand_button3:

              if (expandableLayout3.isExpanded()) {
                  expandableLayout2.collapse();
                  expandableLayout1.collapse();
                  expandableLayout4.collapse();
              } else {

                  expandableLayout3.expand();
              }

              break;
          case R.id.expand_button4:

              if (expandableLayout4.isExpanded()) {
                  expandableLayout2.collapse();
                  expandableLayout3.collapse();
                  expandableLayout1.collapse();
              } else {

                  expandableLayout4.expand();
              }

              break;
      }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_map) {
            startActivity(new Intent(getApplicationContext(), MapsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
