package com.pynch.zacl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


    RequestQueue requestQueue;

    private static final String TAG = NewsActivity.class.getSimpleName();
    // driver json url
    private static final String url = "http://www.zacl.co.zm/index.php/api/get/filter";
    private ProgressDialog pDialog;
    private List<Api> apiList= new ArrayList<>();
    private ListView listView;
    private ApiAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = findViewById(R.id.listView);
        adapter = new ApiAdapter(this, apiList);

        listView.setOnItemClickListener(this);


        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();

        requestQueue = Volley.newRequestQueue(this);



        // Creating volley request obj
        JsonArrayRequest apiReq = new JsonArrayRequest(url,
                response -> {
                    Log.d(TAG, response.toString());
                    hidePDialog();

                    // Parsing json
                    for (int i = 0; i < response.length(); i++) {
                        try {

                            JSONObject obj = response.getJSONObject(i);
                            Api api = new Api();
                            String title = obj.getString("title");
                            api.setKey(title);
                            api.setValue(obj.getString("content"));
                            api.setLink(obj.getString("link"));


                            apiList.add(api);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();
                }, error -> {
            VolleyLog.d(TAG, "Error: " + error.getMessage());

            Toast.makeText(getApplicationContext(), "Error",Toast.LENGTH_LONG).show();
            hidePDialog();

        });



        AppController.getInstance().addToRequestQueue(apiReq);


        listView.setAdapter(adapter);


    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent detail = new Intent(getApplicationContext(), NewsDetailActivity.class);
        detail.putExtra("title",apiList.get(i).getKey());
        detail.putExtra("link",apiList.get(i).getLink());
        startActivity(detail);
    }
}
