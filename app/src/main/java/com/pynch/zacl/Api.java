package com.pynch.zacl;

/**
 * Created by Hermann on 4/4/2018.
 */

public class Api {
    private String key;

    public Api(String key, String value, String link) {
        this.key = key;
        Value = value;
        this.link = link;
    }
    public Api(){

    }

    private String Value;

    private String link;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
