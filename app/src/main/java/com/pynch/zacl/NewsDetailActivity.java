package com.pynch.zacl;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class NewsDetailActivity extends AppCompatActivity {
    private  Bundle bundle;

    private String postUrl, title;

    private TextView mTitle, mDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.txtTitle);
        mDetail = findViewById(R.id.txtDetails);

        bundle = getIntent().getExtras();

        title = bundle.getString("title");
        postUrl = bundle.getString("link");


        mTitle.setText(Html.fromHtml(title));
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        new GetRelease().execute();
    }
    private class GetRelease extends AsyncTask<Void, Void, Void> {

        String release, text, linkText,paragraphInner;
        Elements paragraph;

        @Override
        protected Void doInBackground(Void... voids) {

            try {

                Document document = Jsoup.connect(postUrl).get();

                release = document.title();

                paragraph = document.select("p");


                paragraphInner = paragraph.html();

                text = document.body().text(); // "An example link"
                linkText = paragraph.text();

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mDetail.setText(Html.fromHtml(paragraphInner));
            super.onPostExecute(aVoid);
        }
    }

}
