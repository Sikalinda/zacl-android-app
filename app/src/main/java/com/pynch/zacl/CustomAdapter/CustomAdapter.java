package com.pynch.zacl.CustomAdapter;

import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pynch.zacl.R;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private List<ListModel> flightList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView airportName, arrivalTime,departedTime,arrivedText,airportCode,flightCode,airline;

        public MyViewHolder(View view) {
            super(view);
            airportName = (TextView) view.findViewById(R.id.airportName);
            arrivalTime = (TextView) view.findViewById(R.id.arrivalTime);
            departedTime = (TextView) view.findViewById(R.id.departedTime);
            arrivedText = (TextView) view.findViewById(R.id.arrivedText);
            airportCode = (TextView) view.findViewById(R.id.airportCode);
            flightCode = (TextView) view.findViewById(R.id.flightCode);
            airline = (TextView) view.findViewById(R.id.airline);

        }
    }


    public CustomAdapter(List<ListModel> flightList) {
        this.flightList = flightList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_flight_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //airportName, arrivalTime,departedTime,arrivedText,airportCode,flightCode;
        ListModel flight = flightList.get(position);
        holder.airportName.setText(flight.getAirportName());
        holder.arrivalTime.setText(flight.getArrivalTime());
        holder.departedTime.setText(flight.getDepartedTime());
        holder.arrivedText.setText(flight.getArrivedText());
        holder.airportCode.setText(flight.getAirportCode());
        holder.flightCode.setText(flight.getFlightCode());
        holder.airline.setText(flight.getAirline());
    }

    @Override
    public int getItemCount() {
        return flightList.size();
    }
}