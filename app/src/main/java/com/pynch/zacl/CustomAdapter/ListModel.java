package com.pynch.zacl.CustomAdapter;

/**
 * Created by ALU STIUDENT on 5/29/2018.
 */

public class ListModel {

    public ListModel(String airportName, String arrivalTime, String departedTime, String arrivedText, String airportCode, String flightCode,String airline)
    {
        this.airportName = airportName;
        this. arrivalTime = arrivalTime;
        this.departedTime = departedTime;
        this. arrivedText = arrivedText;
        this. airportCode = airportCode;
        this. flightCode = flightCode;
        this.airline = airline;
    }

    private String airportName;
    private String arrivalTime;
    private String departedTime;
    private String arrivedText;
    private String airportCode;
    private String flightCode;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    private String airline;

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartedTime() {
        return departedTime;
    }

    public void setDepartedTime(String departedTime) {
        this.departedTime = departedTime;
    }

    public String getArrivedText() {
        return arrivedText;
    }

    public void setArrivedText(String arrivedText) {
        this.arrivedText = arrivedText;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }
}
